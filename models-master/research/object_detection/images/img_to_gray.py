import os
import cv2

def get_files(root, file_types=['.jpg','.jpeg','.png']):
    files   = []
    for fname in os.listdir(root):
        full_path = os.path.join(root, fname)
        if os.path.isfile(full_path) and os.path.splitext(full_path)[-1].lower() in file_types:
            files.append(full_path)
    return files

def convert(fpath):
    try:
        img = cv2.imread(fpath)
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        cv2.imwrite(fpath, gray)
    except Exception as e:
        print(e, ' in file: ', fpath)


if __name__ == '__main__':
    files = get_files('train')
    for fpath in files:
        print(fpath)
        convert(fpath)
    files = get_files('test')
    for fpath in files:
        print(fpath)
        convert(fpath)
    print('Done')